//
// Source file for rock paper scissor game
// ************************************************


// Collect choices in an array
const choiceArray = ["rock", "paper", "scissor"]; 


function getComputerChoice(choiceArray){
  // Expected input: an array of strings 
  // Expected output: a string
  const randomIndex = Math.floor(Math.random() * choiceArray.length);
  const computerChoice = choiceArray[randomIndex];
  return computerChoice;
}


function getPlayerChoice(choiceArray) {
  // Expected input: an array of strings 
  // Expected output: a string 
  //TODO: Provide a list to select from instead of typing;
  let playerChoice = prompt("Choose rock/paper/scissor").toLowerCase();
  return playerChoice;
}


function playOneRound(computerSelection, playerSelection){
  // Expected input: two strings 
  // Expected output: an array of two integers and one string 
  let decisionText = '';
  let computerScore = 0;
  let playerScore = 0;
  if (computerSelection == "rock") {
    if (playerSelection == "scissor") {
      computerScore = 1; 
    }
    else if (playerSelection == "paper") {
      playerScore = 1;
    }
    else if (playerSelection == "rock") {
    }
  }
  else if (computerSelection == "paper") {
    if (playerSelection == "rock") {
      computerScore = 1; 
    }
    else if (playerSelection == "scissor") {
      playerScore = 1;
    }
    else if (playerSelection == "paper") {
    }
  }
  else if (computerSelection == "scissor") {
    if (playerSelection == "paper") {
      computerScore = 1;
    }
    else if (playerSelection == "rock") {
      playerScore = 1;
    }
    else if (playerSelection == "scissor") {
    }
  }
  if (playerScore > computerScore) {
    decisionText="You win!";
  }
  else if (computerScore > playerScore) {
    decisionText="You lost!";
  }
  else if (computerScore == playerScore) {
    decisionText="It's a draw!";
  }
  else {
    decisionText = "Sorry, there was an unexpected error! Come back later";
  }
  //console.log(computerSelection); // to debug
  //console.log(playerSelection); // to debug
  //console.log(decisionText); // to debug
  return [computerScore, playerScore, decisionText]; 
}


function game() {
  // Expected input: no inputs expected 
  // Expected output: no outputs expected
  let numberOfGames = 5;
  let currentGameResult = 0; 
  let computerChoices = new Array(numberOfGames);
  let playerChoices = new  Array(numberOfGames);
  let computerScores = new Array(numberOfGames);
  let playerScores = new Array(numberOfGames);
  let gameResults = new Array(numberOfGames);

  for (let i = 0; i < numberOfGames; i++) {
    computerChoices[i] = getComputerChoice(choiceArray);
    playerChoices[i] = getPlayerChoice(choiceArray); 
    currentGameResult= playOneRound(computerChoices[i], playerChoices[i]);
    computerScores[i] = currentGameResult[0];
    playerScores[i] = currentGameResult[1];
    gameResults[i] = currentGameResult[2];
    console.log(computerScores[i] + " " + gameResults[i] + " " + playerScores[i] );
  }

}


game();
